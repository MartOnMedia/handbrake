FROM alpine:edge

#RUN sed -i -e 's/v3\.4/edge/g' /etc/apk/repositories
#RUN apk upgrade --update-cache --available
RUN apk add --update handbrake \
    --update-cache \
    --repository http://dl-3.alpinelinux.org/alpine/edge/testing/
#    --allow-untrusted
#RUN apk add --update handbrake 

VOLUME /data
WORKDIR /data

ENTRYPOINT ["HandBrakeCLI"]

